module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? '/' + process.env.CI_PROJECT_NAME + '/'
      : '/'
  }
  module.exports.plugins = (module.exports.plugins || []).concat([
    // below is the plugin you need to add
    new HtmlWebpackPlugin({
      filename: path.resolve(__dirname, './dist/index.html'), // the path where is the `index.html` generated.
      template: 'index.html',
      inject: true,
      minify: {
        removeComments: true,
        collapseWhitespace: true,
        removeAttributeQuotes: true
      },
    }),
  ])