import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios'


Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        todos: [],
        visible: false
    },
    mutations: {
        GET_TODOS: (state, todos) => {
            state.todos = todos.data.data
        },
        ADD_TODO: (state, todo) => {
            state.todos.unshift(todo)
        },
        PUT_TODO: (state, index, new_todo) => {
            state.todos.splice(index, 1, new_todo)
        },
        DEL_TODO: (state, index) => {
            state.todos.splice(index, 1)
        },
        SHW_MOD: (state, visible) => {
                visible = true
            state.visible = visible
        },
        CLS_MOD: (state, visible) => {
                visible = false
            state.visible = visible
        }
    },
    actions: {
        GET_TODOLIST({commit}) {
            axios
            .get('https://fiber-project.herokuapp.com/api/todo')
            .then((todos)=>{
                commit('GET_TODOS', todos)
            })
            .catch((error)=>{
                console.log(error)
                return error
            })
        },

        CREATE_TODO({commit}, todo) {
            axios
            .post('https://fiber-project.herokuapp.com/api/todo', todo)
            .then(commit('ADD_TODO', todo))
            .catch((error) =>{
                console.log(error)
            })
        },
        UPDATE_TODO({commit},index, new_todo) {
            commit('PUT_TODO', index, new_todo)
        },

        DELETE_TODO({commit}, index) {
            commit('DEL_TODO', index)
        },

        SHOW({commit}, visible) {
            commit('SHW_MOD', visible)
        },
        CLOSE({commit}, visible) {
            commit('CLS_MOD', visible)
        },

    },
    getters: {
        TODOS(state) {
            return state.todos;
        },
        VISIBLE(state) {
            return state.visible;
        },
    }
});

export default store;