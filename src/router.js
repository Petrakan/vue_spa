import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/views/Home.vue';
import Todos from '@/views/Todos.vue'

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/todo',
      name: 'todos',
      component: Todos
    }
  ]
});
